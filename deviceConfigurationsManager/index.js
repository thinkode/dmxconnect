const fs = require('fs')

var configurationPath = 'devices/conf';
var configurationExtension = '.json';

/*Classe qui définit la configuration d'un appareil*/
var DeviceConfiguration = class {
    constructor(){
        this.slug = "default";
        this.name = "Default name";
        this.model = "Default model";
        this.manufacturer = "Default manufacturer";
        this.channelModes = [];
    }

    //Récupère les infos basiques d'une configuration d'appareil
    getInfo(){
        var data = {};
        data.slug = this.slug;
        data.name = this.name;
        data.model = this.model;
        data.manufacturer = this.manufacturer;
        return data;
    }

    //Charge une configuration depuis un fichier
    loadConfiguration(slug){
        let fileSlug = (slug == "") ? this.slug : slug;
        try{
            var data = fs.readFileSync(configurationPath + "/" + fileSlug + configurationExtension);
            data = JSON.parse(data);
            this.name = data.name;
            this.model = data.model;
            this.manufacturer = data.manufacturer;
            this.channelModes = data.channelModes;
            this.slug = fileSlug;
        }
        catch (e){
            throw new Error("An error has occured when trying to read the device configuration.");
        }
    }

    //Sauvegarde une configuration dans un fichier
    saveConfiguration(slug){
        let fileSlug = (slug == "") ? this.slug : slug;
        try{
            var data = {};
            data.name = this.name;
            data.model = this.model;
            data.manufacturer = this.manufacturer;
            data.channelModes = this.channelModes;
            fs.writeFileSync(configurationPath + "/" + fileSlug + configurationExtension, JSON.stringify(data));
            this.slug = fileSlug;
        }
        catch (e){
            throw new Error("An error has occured when trying to save the device configuration.");
        }
    }
}

/*Classe qui gère la configuration des appareils*/
var DeviceConfigurations = class {
    constructor(){
        this.deviceConfigurations = [];
        try {
            this.getAllConfigurations();
        }
        catch (e) {
            throw new Error(e);
        }
    }

    //Récupère tous les appareils configurés
    getAllConfigurations(){
        try{
            let filenames = fs.readdirSync(configurationPath);
            filenames.forEach((file) => {
                var deviceConfiguration = new DeviceConfiguration();
                deviceConfiguration.loadConfiguration(file.substring(0, file.length-configurationExtension.length));
                this.deviceConfigurations.push(deviceConfiguration);
            });
        }
        catch (e){
            throw new Error("An error has occured when trying to get the configured devices.");
        }
    }

    //Récupère les infos basiques de tous les appareils
    listAllConfigurations(){
        let data = [];
        this.deviceConfigurations.forEach(function(deviceConfiguration){
            data.push(deviceConfiguration.getInfo());
        });
        return JSON.stringify(data);
    }

    //Récupère un appareil en fonction de son slug
    getConfiguration(slug){
        let device = false;
        this.deviceConfigurations.forEach((deviceConfiguration) => {
            if(deviceConfiguration.slug == slug) device = deviceConfiguration;
        });
        return device;
    }
}

module.exports = DeviceConfigurations;