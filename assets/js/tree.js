$.fn.extend({
    treed: function (o) {

      var openedClass = 'fa-minus-circle bx bx-chevron-down';
      var closedClass = 'fa-plus-circle bx bx-chevron-right';

      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            console.log($(this));
            //Ajout du li en tant que branche
            var branch = $(this);
            branch.addClass('branch');

            //Ajout de l'icône pour ouvrir/fermer
            var branchContent = $(this).find('div').find('p');
            branchContent.prepend("<i class='indicator fas " + openedClass + "'></i>");

            //Ajout de l'événement pour ouvrir/fermer la branche
            branchContent.on('dblclick', function (e) {
                if (this == e.target) {
                    var icon = branchContent.find('i');
                    icon.toggleClass(openedClass + " " + closedClass);
                    branch.children('ul').children().toggle();
                }
            })
        });
        //fire event from the dynamically added icon
        tree.find('.branch .indicator').each(function(){
            //Ajout de l'icône pour ouvrir/fermer
            var branchContent = $(this).find('div').find('p');
            branchContent.on('dblclick', function () {
                $(this).closest('li').click();
            });
        });

        //fire event to open branch if the li contains an anchor instead of text
        /*tree.find('.branch>a').each(function () {
            $(this).on('dblclick', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('dblclick', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });*/
    }
});