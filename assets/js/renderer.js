
document.addEventListener("DOMContentLoaded", function(event) {
    
    const {ipcRenderer} = require('electron');
    const Toastify = require('toastify-js')
    const showNavbar = (toggleId, navId, bodyId, headerId) =>{
        const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        bodypd = document.getElementById(bodyId),
        headerpd = document.getElementById(headerId)
        
        // Validate that all variables exist
        if(toggle && nav && bodypd && headerpd){
            toggle.addEventListener('click', ()=>{
                // show navbar
                nav.classList.toggle('show')
                // change icon
                toggle.classList.toggle('bx-x')
                // add padding to body
                bodypd.classList.toggle('body-pd')
                // add padding to header
                headerpd.classList.toggle('body-pd')
            })
        }
    }
    showNavbar('header-toggle','nav-bar','body-pd','header');

    //Test button
    document.getElementById('testButton').addEventListener('click', function(){
        ipcRenderer.send('performTest');
    });
    
    /*===== LINK ACTIVE =====*/
    const linkColor = document.querySelectorAll('.nav_link');
    
    function colorLink(){
        if(linkColor){
            linkColor.forEach(l=> l.classList.remove('active'));
            this.classList.add('active');
        }
    }
    linkColor.forEach(l=> l.addEventListener('click', colorLink));

    //Gestion de l'ouverture des pages
    var myMenus = document.querySelectorAll('.menu');
    function changeSheet(){   
        const sheets = document.querySelectorAll('.sheet');
        sheets.forEach(l=> l.classList.remove('sheet-active'));
        let sheet = document.getElementById(this.getAttribute('sheet'));
        sheet.classList.add('sheet-active');
        document.getElementById('page_title').innerHTML = sheet.getAttribute('sheet-name');  
        //Si la page des appareils est cliquée, on actualise la liste
        if(sheet.getAttribute('sheet-name') == "Scene") prepareScene(); 
        else if(sheet.getAttribute('sheet-name') == "Sequencer") prepareSequencer(); 
        else if(sheet.getAttribute('sheet-name') == "Devices") prepareDevices(); 
        log("Page " + this.getAttribute('sheet') + " affichee");
    }
    myMenus.forEach(l=> l.addEventListener('click', changeSheet));

    //Préparer l'affichage de la scène
    function prepareScene(){
        //Recherche des appareils de la bibliothèque
        ipcRenderer.send('allDevicesScene');
        //Affichage de la composition des channels
        ipcRenderer.send('refreshSceneChannels');
    }

    //Préparer l'affichage du séquenceur
    function prepareSequencer(){
        //On charge la liste des séquences
        resetSequenceEditor();
        ipcRenderer.send('allSequences');
    }

    //Préparer l'affichage des appareils
    function prepareDevices(){
        ipcRenderer.send('allDevices');
    }

    ipcRenderer.on('listSequences', (event, sequences) => {
        sequences = JSON.parse(sequences);
        let treeCreator = "";
        //Treeview
        if(sequences.length == 0) document.getElementById('emptySequenceText').innerHTML = "No sequence found. Click on <i class='bx bxs-add-to-queue'></i> to create a sequence.";
        else document.getElementById('emptySequenceText').innerHTML = "";
        //Pour chaque séquence, on crée un objet dans le tableau de données
        sequences.forEach(function(sequence) {
            treeCreator += "<li data-type='sequence' data-id='" + sequence.id + "'><div>";
            treeCreator += "<p class='treeItemHover' style='display:inline-block; margin:0;'>" + sequence.name + "</p>";
            treeCreator += "<button class='playSequence btn btn-dark' sequence-id='" + sequence.id + "' style='float: right; font-size: 1.25em; padding-right: 5px; padding-left: 5px;'><i class='bx bx-play-circle'></i> " + sequence.key + "</button></div>";
            if(sequence.frames.length > 0) {
                treeCreator += "<ul>";
                sequence.frames.forEach(function(frame) {
                    treeCreator += "<li data-type='frame' frame-id='" + frame.id + "' sequence-id='" + sequence.id + "'>";
                    treeCreator += "<div class='treeItemHover' style='margin:0;'>" + frame.name + " (Device:<b>" + frame.deviceData.id + "</b> Address:<b>" + frame.deviceData.device.channel + "</b> - " + frame.deviceData.device.configuration.name + ")</div></li>";
                });
                treeCreator += "</ul>";
            }
            treeCreator += "</li>";
        });
        document.getElementById('sequencesTree').innerHTML = treeCreator;  
        //Initialization of treeviews
        $('#sequencesTree').treed();  
        //Choisir un item
        $('#sequencesTree').find('li').each(function () {
            $(this).on('click', function (e) {
                if($(this).attr('data-type') == "sequence") loadSequence($(this).attr('data-id'));
                else if($(this).attr('data-type') == "frame") loadFrame($(this).attr('sequence-id'), $(this).attr('frame-id'));
                e.stopPropagation();
                e.preventDefault();
            });
        });
        //Bouton de jeu
        document.querySelectorAll('.playSequence').forEach(function(button) {
            button.addEventListener('click', function (e) {
                ipcRenderer.send('playSequence', button.getAttribute('sequence-id'));
                e.stopPropagation();
            });
        });
    });

    //Retire la sélection sur tous les items du treeview
    function resetSelectedTreeItems(){
        var treeItems = document.querySelectorAll('.treeItemSelected');
        treeItems.forEach(function(treeItem) {
            treeItem.classList.remove('treeItemSelected');
        });
    }

    //Charger une séquence dans l'éditeur
    function loadSequence(sequenceId){
        ipcRenderer.send('selectSequence', sequenceId);
    }

    //Sélectionne l'objet séquence dans le treeview
    function selectSequenceItem(sequenceId){
        resetSelectedTreeItems();
        $('#sequencesTree').find('li').each(function () {
            if($(this).attr('data-type') == "sequence" && $(this).attr('data-id') == sequenceId) $(this).find('div:first').addClass('treeItemSelected');
        });
    }

    //Sélectionne l'objet frame dans le treeview
    function selectFrameItem(sequenceId, frameId){
        resetSelectedTreeItems();
        $('#sequencesTree').find('li').each(function () {
            if($(this).attr('data-type') == "frame" && $(this).attr('sequence-id') == sequenceId && $(this).attr('frame-id') == frameId) $(this).find('div:first').addClass('treeItemSelected');
        });
    }

    //Charger une frame dans l'éditeur
    function loadFrame(sequenceId, frameId){
        ipcRenderer.send('selectFrame', sequenceId, frameId);
    }

    ipcRenderer.on('sequenceData', (event, sequenceData) => {
        //On réinitialise l'ancien appareil 
        resetDevice();
        sequenceData = JSON.parse(sequenceData);
        //On sélectionne l'item dans le treeview
        selectSequenceItem(sequenceData.id);
        //On affiche les données de la séquence
        document.getElementById('sequencerEditName').innerHTML = sequenceData.name;
        let panelCreator = "";
        panelCreator += "<h6>Sequence ID: <b>" + sequenceData.id + "</b></h6><br>";
        panelCreator += "<label for='sequenceName' class='form-label'>Sequence name</label>";
        panelCreator += "<input type='text' class='form-control bg-dark' style='border:none; color:#cccccc;' id='sequenceName' placeholder='My sequence' value='" + sequenceData.name + "'><br>";
        panelCreator += "<label for='sequenceKey' class='form-label'>Sequence key</label>";
        panelCreator += "<select id='sequenceKey' class='form-select bg-dark' style='border:none; color:#cccccc;' aria-label='Device selection'>";
        panelCreator += "<option value='' selected>No key</option>";
        panelCreator += "<option value='a'>Letter A</option>";
        panelCreator += "<option value='b'>Letter B</option>";
        panelCreator += "<option value='c'>Letter C</option>";
        panelCreator += "<option value='d'>Letter D</option>";
        panelCreator += "<option value='e'>Letter E</option>";
        panelCreator += "<option value='f'>Letter F</option>";
        panelCreator += "<option value='g'>Letter G</option>";
        panelCreator += "<option value='h'>Letter H</option>";
        panelCreator += "<option value='i'>Letter I</option>";
        panelCreator += "<option value='j'>Letter J</option>";
        panelCreator += "<option value='k'>Letter K</option>";
        panelCreator += "<option value='l'>Letter L</option>";
        panelCreator += "<option value='m'>Letter M</option>";
        panelCreator += "<option value='n'>Letter N</option>";
        panelCreator += "<option value='o'>Letter O</option>";
        panelCreator += "<option value='p'>Letter P</option>";
        panelCreator += "<option value='q'>Letter Q</option>";
        panelCreator += "<option value='r'>Letter R</option>";
        panelCreator += "<option value='s'>Letter S</option>";
        panelCreator += "<option value='t'>Letter T</option>";
        panelCreator += "<option value='u'>Letter U</option>";
        panelCreator += "<option value='v'>Letter V</option>";
        panelCreator += "<option value='w'>Letter W</option>";
        panelCreator += "<option value='x'>Letter X</option>";
        panelCreator += "<option value='y'>Letter Y</option>";
        panelCreator += "<option value='z'>Letter Z</option>";
        panelCreator += "<option value='0'>Numpad 0</option>";
        panelCreator += "<option value='1'>Numpad 1</option>";
        panelCreator += "<option value='2'>Numpad 2</option>";
        panelCreator += "<option value='3'>Numpad 3</option>";
        panelCreator += "<option value='4'>Numpad 4</option>";
        panelCreator += "<option value='5'>Numpad 5</option>";
        panelCreator += "<option value='6'>Numpad 6</option>";
        panelCreator += "<option value='7'>Numpad 7</option>";
        panelCreator += "<option value='8'>Numpad 8</option>";
        panelCreator += "<option value='9'>Numpad 9</option>";
        panelCreator += "</select><br>";
        panelCreator += "<button id='sequenceAddFrameBtn' class='btn btn-dark' data-id='" + sequenceData.id + "' style='width:100%;'>Add frame</button><br><br>";
        panelCreator += "<button id='sequenceUpdateBtn' class='btn btn-success' data-id='" + sequenceData.id + "' style='width:100%;'>Update</button>";
        panelCreator += "<button id='sequenceDeleteBtn' class='btn btn-danger' data-id='" + sequenceData.id + "' style='width:100%;'>Delete</button>";
        document.getElementById('sequencerEditPanel').innerHTML = panelCreator;
        //Sélection de la touche de jeu
        document.getElementById('sequenceKey').value = sequenceData.key;
        //Mettre à jour la séquence
        document.getElementById('sequenceUpdateBtn').addEventListener('click', function(){
            ipcRenderer.send('updateSequence', this.getAttribute('data-id'), document.getElementById('sequenceName').value, document.getElementById('sequenceKey').value);
        });
        //Supprimer la séquence
        document.getElementById('sequenceDeleteBtn').addEventListener('click', function(){
            ipcRenderer.send('deleteSequence', this.getAttribute('data-id'));
            resetSequenceEditor();
        });
        //Ajouter un frame à la séquence
        document.getElementById('sequenceAddFrameBtn').addEventListener('click', function(){
            ipcRenderer.send('addFrame', this.getAttribute('data-id'));
        });
    });

    //Charger une frame dans l'éditeur
    let deviceShowing = undefined;
    ipcRenderer.on('frameData', (event, sequenceId, frameData, availableDevice) => {
        //Si l'ancien appareil est différent de celui qu'on s'apprête à ouvrirn, on réinitialise l'ancien appareil 
        frameData = JSON.parse(frameData);
        availableDevice = JSON.parse(availableDevice);
        if(deviceShowing != frameData.deviceId) resetDevice();
        //On sélectionne l'item dans le treeview
        selectFrameItem(sequenceId, frameData.id);
        //On affiche les données de la frame
        document.getElementById('sequencerEditName').innerHTML = frameData.name;
        let panelCreator = "";
        panelCreator += "<h6>Frame ID: <b>" + frameData.id + "</b> from sequence ID: <b>" + sequenceId + "</b></h6><br>";
        panelCreator += "<label for='frameName' class='form-label'>Frame name</label>";
        panelCreator += "<input type='text' class='form-control bg-dark' style='border:none; color:#cccccc;' id='frameName' placeholder='My frame' value='" + frameData.name + "'><br>";
        panelCreator += "<label for='frameDevice' class='form-label'>Frame device</label>";
        panelCreator += "<select id='frameDevice' class='form-select bg-dark' style='border:none; color:#cccccc;' aria-label='Device selection'>";
        panelCreator += "<option value='-1'>No device selected</option>";
        availableDevice.forEach(device => {
            panelCreator += "<option device-address='" + device.channel + "' value='" + device.id + "'>CH: " + device.channel + " - " + device.name + "</option>";
        });
        panelCreator += "</select><br>";
        panelCreator += "<p>Channel values</p><div id='sequencerChannels'></div>";
        panelCreator += "<button id='frameUpdateBtn' class='btn btn-success' sequence-id='" + sequenceId + "' frame-id='" + frameData.id + "' style='width:100%; margin-top: 10px;'>Update</button>";
        panelCreator += "<button id='frameDeleteBtn' class='btn btn-danger' sequence-id='" + sequenceId + "' frame-id='" + frameData.id + "' style='width:100%;'>Delete</button>";
        document.getElementById('sequencerEditPanel').innerHTML = panelCreator;
        //Mettre à jour la frame
        document.getElementById('frameUpdateBtn').addEventListener('click', function(){
            let name = document.getElementById('frameName').value;
            let device = document.getElementById('frameDevice').value;
            let channels = [];
            if(device != -1){
                document.querySelectorAll('.channelValue').forEach(channel => {
                    channels.push(channel.value);
                });
            }
            ipcRenderer.send('updateFrame', sequenceId, frameData.id, name, device, channels);
        });
        //Supprimer la frame
        document.getElementById('frameDeleteBtn').addEventListener('click', function(){
            ipcRenderer.send('deleteFrame', sequenceId, this.getAttribute('frame-id'));
            resetSequenceEditor();
        });
        let deviceBox = document.getElementById('frameDevice').options;
        for(let i = 0; i < deviceBox.length; i++){
            if(deviceBox[i].value == frameData.deviceId) deviceBox[i].selected = true;
        }
        deviceShowing = frameData.deviceId;
        loadChannels(frameData.values);
        //On change d'appareil sélectionné
        document.getElementById('frameDevice').addEventListener('change', function(){
            deviceShowing = this.value;
            loadChannels([]);
        });
    });

    //Charger les canaux disponibles pour l'appareil sélectionné dans le séquenceur
    function loadChannels(values){
        let deviceId = document.getElementById('frameDevice').value;
        if(deviceId == -1) document.getElementById('sequencerChannels').innerHTML = "<p>Please choose a device to edit his channels.</p>";
        else ipcRenderer.send('availableChannels', deviceId, values);
    }
    ipcRenderer.on('channelsData', (event, data, values) => {
        data = JSON.parse(data);
        channelsData = data.channels;
        let channelsCreator = "";
        let channelId = 0;
        channelsData.forEach(channel => {
            channelsCreator += "<h3 style='display:inline-block; margin-top: 10px;'><i class='bx bxs-cog'></i> " + channel.name + "</h3>";
            channelsCreator += "<div style='display:inline-block; float:right;margin-top:7px;' class='col-3'>";
            channelsCreator += "<input type='number' channel-id='" + channelId + "' value='0' min='0' max='255' step='1' id='valueChannel" + channelId + "' class='channelValue form-control bg-dark' style='text-align:center; color:#cccccc; border:none;'></div>";
            channelsCreator += "<div style='background-color:#252525; margin-top:10px; padding:5px; border-radius:5px;'>";  
            channelsCreator += "<select id='selectorRangeChannel" + channelId + "' channel-id='" + channelId + "' class='rangeSelector form-select bg-dark' style='border:none; color:#cccccc;'>";
            let rangeId = 0;
            channel.ranges.forEach(range => {
                channelsCreator += "<option value='" + rangeId + "'>" + range.name + "</option>";
                rangeId++;
            });
            channelsCreator += "</select>";
            channelsCreator += "<input id='sliderChannel" + channelId + "' channel-id='" + channelId + "' type='range' min='" + channel.ranges[0].fromValue + "' max='" + channel.ranges[0].toValue + "' step='1' value='" + channel.ranges[0].fromValue + "' class='form-range rangeSlider' id='range'>";
            channelsCreator += "<div id='infoChannel" + channelId + "' style='font-size:0.7em;'><i>" + channel.ranges[0].fromLabel + "(" + channel.ranges[0].fromValue + ")</i><i style='float:right;'>" + channel.ranges[0].toLabel + "(" + channel.ranges[0].toValue + ")</i></div></div>";
            channelId++;
        });
        document.getElementById('sequencerChannels').innerHTML = channelsCreator;
        //On complète les données de la frame
        if(values.length == 0) values = new Array(channelsData.length).fill(0);
        for(let i = 0; i < channelsData.length; i++){
            document.getElementById('valueChannel' + i).value = values[i];
            updateValuesFromInput(i, values[i]);
        }
        //On enregistre l'événement change sur tous les sélecteurs de range
        function changeSlider(channelId, rangeId){
            //Changement du slider
            let channelSlider = document.getElementById('sliderChannel' + channelId);
            channelSlider.setAttribute('min', channelsData[channelId].ranges[rangeId].fromValue);
            channelSlider.setAttribute('max', channelsData[channelId].ranges[rangeId].toValue);
            //Changement des infos slider
            document.getElementById('infoChannel' + channelId).innerHTML = "<i>" + channelsData[channelId].ranges[rangeId].fromLabel + "(" + channelsData[channelId].ranges[rangeId].fromValue + ")</i><i style='float:right;'>" + channelsData[channelId].ranges[rangeId].toLabel + "(" + channelsData[channelId].ranges[rangeId].toValue + ")</i>";
        }
        document.querySelectorAll('.rangeSelector').forEach(function(element){
            element.addEventListener('change', function(){
                let channelId = this.getAttribute('channel-id');
                let rangeId = this.value;
                changeSlider(channelId, rangeId);
                document.getElementById('sliderChannel' + channelId).value = channelsData[channelId].ranges[rangeId].fromValue;
                document.getElementById('valueChannel' + channelId).value = channelsData[channelId].ranges[rangeId].fromValue;
                updateChannel(channelId, channelsData[channelId].ranges[rangeId].fromValue);
            });
        });
        //On met à jour la valeur de canal à partir du slider
        document.querySelectorAll('.rangeSlider').forEach(function(element){
            element.addEventListener('mousedown', function(e){
                function onMouseMove(e) {
                    let channelId = element.getAttribute('channel-id');
                    document.getElementById('valueChannel' + channelId).value = element.value;
                    updateChannel(channelId, element.value);
                }
                element.addEventListener('mousemove', onMouseMove);
                element.onmouseup = function(e) {
                    element.removeEventListener('mousemove', onMouseMove);
                    element.onmouseup = null;
                };
                element.onclick = onMouseMove;
            });
        });
        //On met à jour la valeur du canal à partir du champ de saisie
        document.querySelectorAll('.channelValue').forEach(function(element){
            element.addEventListener('change', function(e){
                updateValuesFromInput(element.getAttribute('channel-id'), element.value);
            });
        });

        function updateValuesFromInput(channelId, value){
            //Affichage de la range correspondante
            let rangeId = 0;
            channelsData[channelId].ranges.forEach(range => {
                if(value >= range.fromValue && value <= range.toValue) {
                    document.getElementById('selectorRangeChannel' + channelId).options[rangeId].selected = true;
                    changeSlider(channelId, rangeId);
                }
                rangeId++;
            });
            //Changement de la valeur du slider
            document.getElementById('sliderChannel' + channelId).value = value;
            updateChannel(channelId, value);
        }

        function updateChannel(channelId, value){
            //Changement de la valeur du champ de saisie
            ipcRenderer.send('updateChannelValue', parseInt(channelId) + data.address, value);
        }
    });

    //Ajouter une nouvelle séquence
    document.getElementById('addSequence').addEventListener('click', function(){
        ipcRenderer.send('addSequence');
    });

    //Remet à zéro l'éditeur de séquence
    function resetSequenceEditor(){
        //On réinitialise l'ancien appareil 
        resetDevice();
        document.getElementById('sequencerEditName').innerHTML = "No item selected";
        document.getElementById('sequencerEditPanel').innerHTML = "Click an item to edit it.";
    }

    //Réinitialisation de l'ancien appareil
    function resetDevice(){
        if(deviceShowing != undefined && deviceShowing != -1) ipcRenderer.send('resetDevice', deviceShowing);
        deviceShowing = undefined;
    }

    //Fonctionnalité de contrôle des touches
    var playMode = document.getElementById('runPlayMode');
    playMode.addEventListener('keydown', function(e){
        ipcRenderer.send('playSequenceWithKey', e.key);
    });
    //Entrée dans le mode play
    playMode.addEventListener('click', function(e){
        Toastify({
            text: "Mode scénario activé. Cliquez sur une touche pour jouer une séquence.",
            duration: 3000,
            close: true,
            gravity: "bottom", // `top` or `bottom`
            position: "right", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: { background: "#252526", color: "#CCCCCC" }
        }).showToast();        
    });
    //Sortie du mode play
    playMode.addEventListener('focusout', function(e){
        Toastify({
            text: "Mode scénario désactivé. Cliquez sur le bouton play pour le réactiver.",
            duration: 3000,
            close: true,
            gravity: "bottom", // `top` or `bottom`
            position: "right", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: { background: "#252526", color: "#CCCCCC" }
        }).showToast(); 
    });

    ipcRenderer.on('listDevices', (event, devices) => {
        devices = JSON.parse(devices);
        let tableCreator = "";
        devices.forEach(function(device) {
            tableCreator += "<tr id='" + device.slug + "'><th><img src='./../../devices/img/" + device.slug + ".png' style='width:32px; height:32px; border-radius:5px;' alt='...'></th>";
            tableCreator += "<td>" + device.name + "</td>";
            tableCreator += "<td>" + device.model + "</td>";
            tableCreator += "<td>" + device.manufacturer + "</td>";
            tableCreator += "<td><button id='" + device.slug + "' type='button' class='btn btn-dark'><i class='bx bx-edit-alt'></i></button></td></tr>";
        });
        document.getElementById('devicesTable').innerHTML = tableCreator;
    });

    ipcRenderer.on('listDevicesScene', (event, devices) => {
        devices = JSON.parse(devices);
        let listCreator = "";
        devices.forEach(function(device) {           
            listCreator += "<div class='devicesLibrary deviceCard' device='" + device.slug + "'>";
            listCreator += "<img src='./../../devices/img/" + device.slug + ".png' alt='...'>";
            listCreator += "<div style=''>";
            listCreator += "<h5 style='text-align:center;'>" + device.name + "</h5>";
            listCreator += "<p style='text-align:center;'>" + device.model + "</p></div></div>"; 
        });
        document.getElementById('devicesList').innerHTML = listCreator;
        dragDeviceToScene();
    }); 

    //Activation du drag and drop
    let deviceDragged = "";
    function dragDeviceToScene(){
        let devices = document.querySelectorAll('.devicesLibrary');
        devices.forEach((device) => {
            device.onclick = function(){
                ipcRenderer.send('addDeviceToScene', device.getAttribute('device'));                
            }
        });
    }  

    ipcRenderer.on('showToast', (event, message) => {
        Toastify({
            text: message,
            duration: 3000,
            close: true,
            gravity: "bottom", // `top` or `bottom`
            position: "right", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: { background: "#252526", color: "#CCCCCC" }
        }).showToast();
    });

    ipcRenderer.on('sceneChannelsInfo', (event, sceneInfo) => {
        
        let devicesNumber = sceneInfo.devices.length;
        let conflictsNumber = sceneInfo.conflicts.length;

        let info = "<p class='btn btn-dark'>Devices <span class='badge badge-light'>" + devicesNumber + "</span></p>";
        info += "<p class='btn btn-dark'>Conflicts <span class='badge badge-light'>" + conflictsNumber + "</span></p>";
        
        document.getElementById('lblSceneInfo').innerHTML = info;
        document.getElementById('lblSceneChannels').innerHTML = "";
        document.getElementById('scene_content').innerHTML = "";
        let id = 0;
        sceneInfo.devices.forEach(function(device) {  
            //Affichage des infos de la scène
            var itemCreator = "<tr class='channelRows' device='" + id + "'><th scope='row'>" + id + "</th>";
            itemCreator += "<td><div class='input-group mb-3'><input id='channel_" + id + "' class='channelInput form-control bg-dark' style='border:none; color:#cccccc;' placeholder='Channel' aria-label='Channel' aria-describedby='basic-addon2' type='number' name='channel' min='1' max='" + sceneInfo.maxChannel + "' value='" + device.channel + "'>";
            itemCreator += "<div class='input-group-append'><button device='" + id + "' class='saveDeviceChannel btn btn-success'><i class='bx bx-check'></i></button></div></div></td>";
            itemCreator += "<td><button device='" + id + "' class='executeTest btn btn-dark' disabled><i class='bx bx-play'></i></button></td></tr>";  
            document.getElementById('lblSceneChannels').innerHTML += itemCreator;
            //Affichage des appareils DMX graphiquement
            itemCreator = "<div class='devicesScene deviceShape' device='" + id + "' style='position:absolute; background-image:url(./../../devices/img/" + device.slug + ".png); left:" + device.x + "%; top:" + device.y + "%;'>;";
            itemCreator += "<h3 class='segmentFont'>CH" + device.channel + "</h3></div>";
            document.getElementById('scene_content').innerHTML += itemCreator;
            id++;
        });
        //Réouverture du volet Channels pour éviter le tronquage des infos
        document.getElementById('btnChannelsPanel').click();
        if(!document.getElementById('btnChannelsPanel').classList.contains('activeAccordion')) document.getElementById('btnChannelsPanel').click();
        setClickOnButtons();
    });

    //Arrêter de bouger lorsque la touche Echap est pressée
    document.getElementById('scene_content').onkeydown = function(key){
        document.getElementById('scene_content').removeEventListener('mousemove', onMouseMove);
    };

    //Attribue les clics sur les boutons de la partie channels
    function setClickOnButtons(){
        //Attribution des clics pour boutons test
        var testButtons = document.querySelectorAll('.executeTest');
        testButtons.forEach(function(testButton){
            testButton.addEventListener('click', function(){
                ipcRenderer.send('executeTest', this.getAttribute('device'));
            });
        });
        //Pour chaque zone de saisie on traite l'évenement change
        var channelInputs = document.querySelectorAll('.channelInput');
        channelInputs.forEach(function(channelInput){
            channelInput.classList.remove('bg-danger');
            channelInput.classList.add('bg-dark');
            channelInput.oninput = function(){
                channelInput.classList.add('bg-danger');
                channelInput.classList.remove('bg-dark');
            };
        });
        //Attribution des clics pour valeur de canal
        var channelButtons = document.querySelectorAll('.saveDeviceChannel');
        channelButtons.forEach(function(channelButton){
            channelButton.addEventListener('click', function(){
                let deviceId = this.getAttribute('device');
                let inputValue = document.getElementById('channel_' + deviceId);
                ipcRenderer.send('setDeviceChannel', deviceId, parseInt(inputValue.value));
            });
        });
        //Evenements sur les deviceShape
        let deviceShapes = document.querySelectorAll('.devicesScene');
        deviceShapes.forEach(function(deviceShape){
            //Appui sur l'appareil, on déclenche le mouvement
            deviceShape.onmousedown = function(event) { 
                function onMouseMove(event) {
                    //On récupère le pourcentage de marge à appliquer
                    let pointX = event.clientX;
                    let sceneWidth = document.getElementById('scene_content').offsetWidth;
                    let pointY = event.clientY; 
                    let sceneHeight = document.getElementById('scene_content').offsetHeight;

                    let percentX = 100*(pointX - 164)/sceneWidth;
                    let percentY = 100*(pointY - 164)/sceneHeight;

                    //On bouge l'appareil
                    deviceShape.style.left = percentX + "%";
                    deviceShape.style.top = percentY + "%";

                    //On sauvegarde ses coordonnées
                    ipcRenderer.send('saveCoordinates', deviceShape.getAttribute('device'), percentX, percentY);
                }
                document.getElementById('scene_content').addEventListener('mousemove', onMouseMove);

                deviceShape.onmouseup = function(event) {
                    document.getElementById('scene_content').removeEventListener('mousemove', onMouseMove);
                    deviceShape.onmouseup = null;
                };
            };
            deviceShape.onclick = function(event){
                deselectDevices();
                deviceShape.classList.add('deviceSelected');
                ipcRenderer.send('getDeviceInfo', deviceShape.getAttribute('device'));
            }
        });

        //Attribution des clics pour sélectionner un appareil depuis la liste channels
        var channelRows = document.querySelectorAll('.channelRows');
        channelRows.forEach(function(channelRow){
            let deviceId = channelRow.getAttribute('device');
            channelRow.onclick = function(){
                let deviceShapes = document.querySelectorAll('.devicesScene');
                deviceShapes.forEach(function(deviceShape){
                    if(deviceShape.getAttribute('device') == deviceId) deviceShape.click();                    
                });
            };
        });
    }

    //Affiche les données de l'appareil sélectionné
    let isDeviceSelected = false;
    ipcRenderer.on('deviceInfo', (event, device) => {
        let itemCreator = "<div class='deviceCard' device='" + device.id + "'><img src='./../../devices/img/" + device.device.configuration.slug + ".png' alt='...'><div style='text-align:center; padding:10px;'>";
        itemCreator += "<h5>ID: <b>" + device.id + "</b></h5><p>" + device.device.configuration.name + " - " + device.device.configuration.model + "</p><p>Channel mode: ";
        itemCreator += "<select id='deviceChannelMode' style='padding: 5px; border: none; border-radius: 5px; background: #252526; color: #cccccc;'>";
        let channelModeId = 0;
        device.device.configuration.channelModes.forEach(function(channelMode){
            if(device.device.channelMode == channelModeId) itemCreator += "<option selected value='" +  channelModeId + "'>" + channelMode.name + "</option>";
            else itemCreator += "<option value='" +  channelModeId + "'>" + channelMode.name + "</option>";
            channelModeId++;
        });
        itemCreator += "</select></p><button id='deviceDeleteBtn' class='btn btn-danger' device='" + device.id + "' style='width:100%;'>Delete</button></div></div>";
        document.getElementById('devicePanel').innerHTML = itemCreator;
        document.getElementById('btnDevicePanel').click();
        if(!document.getElementById('btnDevicePanel').classList.contains('activeAccordion')) document.getElementById('btnDevicePanel').click();
        //Changement de channel mode d'un appareil
        document.getElementById('deviceChannelMode').onchange = function(event){
            ipcRenderer.send('changeChannelMode', device.id, this.value);
        }
        //Suppression d'un appareil
        document.getElementById('deviceDeleteBtn').onclick = function(event){
            deselectDevices();
            ipcRenderer.send('deleteDevice', device.id);
        }
    });

    //Déselectionne tous les appareils
    function deselectDevices(){
        let deviceShapes = document.querySelectorAll('.deviceShape');
        deviceShapes.forEach(function(deviceShape){
            deviceShape.classList.remove('deviceSelected');
        });
        document.getElementById('devicePanel').innerHTML = "<p style='padding-top:10px;'>Click a device to configure.</p>";
    }

    //Adressage automatique des appareils
    document.getElementById('btnAutomaticAddressing').addEventListener('click', function(){
        ipcRenderer.send('automaticAddressing');
    });

    //Sauvegarde d'un projet
    document.getElementById('saveProjectBtn').addEventListener('click', function(){
        ipcRenderer.send('saveProject');
    });

    //Ouverture d'un projet
    document.getElementById('openProjectBtn').addEventListener('click', function(){
        ipcRenderer.send('openProject');
    });

    //Gestion de l'arrêt de l'application
    document.querySelector("#quit").addEventListener('click', function(){
        ipcRenderer.send('quit');
    });
        
    // Your code to run since DOM is loaded and ready
    var myLink = document.querySelector('a[href="#"]');
    myLink.addEventListener('click', function(e) {
        e.preventDefault();
    });

    //Fonction test pour l'envoi d'un canal
    function write(){
        var number = parseInt(document.getElementById("chooseCanal").value);
        var value = parseInt(document.getElementById("chooseValue").value);
        ipcRenderer.send('canal', number, value);
    }

    //Fonction pour envoyer les logs
    function log(msg, type, timestamp){
        ipcRenderer.send('log', msg, type, true);
    }

    ipcRenderer.on('boxConnected', (event) => {
        document.getElementById("boxStatus").innerHTML = "<span class='badge bg-success'>DMXBox connected</span>";
    });
    ipcRenderer.on('boxDisconnected', (event) => {
        document.getElementById("boxStatus").innerHTML = "<span class='badge bg-danger'>DMXBox disconnected</span>";
    });

    //Gestion des splitters
    document.getElementById("sheet_scene").style.width = window.innerWidth + "px";
	document.getElementById("sheet_scene").style.height = window.innerHeight + "px";
    document.getElementById("sheet_sequencer").style.width = window.innerWidth + "px";
	document.getElementById("sheet_sequencer").style.height = window.innerHeight + "px";
    
    var sizeSceneSplitter = {
        "scene_content": 0.80
    }
    Resizable.initialise("sheet_scene", sizeSceneSplitter);
    var sizeSequencerSplitter = {
        "sequencer_content": 0.75
    }
    Resizable.initialise("sheet_sequencer", sizeSequencerSplitter);
    var offsetSplitters = (document.getElementById('body-pd').offsetWidth > 768) ? 102 : 20;
    Resizable.activeContentWindows[0].changeSize(window.innerWidth - offsetSplitters, window.innerHeight);
    Resizable.activeContentWindows[0].childrenResize();
    Resizable.activeContentWindows[4].changeSize(window.innerWidth - offsetSplitters, window.innerHeight);
    Resizable.activeContentWindows[4].childrenResize();

    window.addEventListener("resize", () => {
        offsetSplitters = (document.getElementById('body-pd').offsetWidth > 768) ? 102 : 20;
        Resizable.activeContentWindows[0].changeSize(window.innerWidth - offsetSplitters, window.innerHeight);
        Resizable.activeContentWindows[0].childrenResize();
        Resizable.activeContentWindows[4].changeSize(window.innerWidth - offsetSplitters, window.innerHeight);
        Resizable.activeContentWindows[4].childrenResize();
    });   
});