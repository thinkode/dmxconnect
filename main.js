// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain, dialog } = require('electron')
const path = require('path')
const fs = require('fs');
var dmxBoxManager = require('./dmxBoxManager')
var deviceConfigurations = require("./deviceConfigurationsManager")
var dmxSceneManager = require('./dmxSceneManager')
var dmxSequencesManager = require('./dmxSequencesManager')

var dmxBox = new dmxBoxManager();
var deviceConfs = new deviceConfigurations();
var dmxScene = new dmxSceneManager(dmxBox);

//Test configuration d'appareil
ipcMain.on('performTest', (event) => {
  dmxBox.getVersion();
});

ipcMain.on('log', (event, msg, type, timestamp) => {
  let date_ob = new Date();
  let date = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  let seconds = date_ob.getSeconds();
  
  var timestampString = (timestamp) ? "[" + year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds + "]": "";
  var typeString = (type == "warning") ? "/!\\" : (type == "danger") ? "!" : "<i>";
  console.log(timestampString, typeString, msg);
});

ipcMain.on('canal', (event, number, value) => {
  dmxBox.writeChannel(number, value);
});

var mainWindow;

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('./assets/html/index.html')
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();
  dmxBox.on('boxConnected', function(event){
    mainWindow.webContents.send('boxConnected');
    mainWindow.webContents.send('showToast', "DMXBox is connected.");
    console.log("DMXBox is connected.");
  });
  dmxBox.on('boxDisconnected', function(event){
    mainWindow.webContents.send('boxDisconnected');
    mainWindow.webContents.send('showToast', "DMXBox is disconnected.");
    console.log("DMXBox is disconnected.");
  });
  dmxBox.on('commandSucceeded', function(event){
    console.log("Command succeeded.");
  });

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  })
})

//Supprime un appareil de la scène
ipcMain.on('deleteDevice', (event, deviceId) => {
  try{ 
    dmxScene.removeDevice(deviceId);
    mainWindow.webContents.send('sceneChannelsInfo', dmxScene.getSceneInfo());
    mainWindow.webContents.send('showToast', "The device has been deleted.");
  }
  catch (e){ mainWindow.webContents.send('showToast', e); }  
});

//Ajoute un appareil à la scène
ipcMain.on('addDeviceToScene', (event, deviceSlug) => {
  try{ 
    dmxScene.addDevice(deviceSlug);
    mainWindow.webContents.send('sceneChannelsInfo', dmxScene.getSceneInfo());
    mainWindow.webContents.send('showToast', "The device has been added to the scene. Please run the automatic addressing to avoid conflicts.");
  }
  catch (e){ mainWindow.webContents.send('showToast', e); }  
});

//Change mode d'un appareil
ipcMain.on('changeChannelMode', (event, deviceId, channelModeId) => {
  try{ 
    dmxScene.setChannelMode(deviceId, channelModeId);
    mainWindow.webContents.send('sceneChannelsInfo', dmxScene.getSceneInfo());
    mainWindow.webContents.send('showToast', "The channel mode of the device " + deviceId + " has been updated.");
  }
  catch (e){ mainWindow.webContents.send('showToast', e); }  
});


//Récupère les données de l'appareil
ipcMain.on('getDeviceInfo', (event, deviceId) => {
  try{ 
    mainWindow.webContents.send('deviceInfo', dmxScene.getDevice(deviceId));
  }
  catch (e){ mainWindow.webContents.send('showToast', e); }  
});

//Sauvegarde de la position d'un appareil
ipcMain.on('saveCoordinates', (event, deviceId, x, y) => {
  try{ 
    dmxScene.moveDevice(deviceId, x, y);
  }
  catch (e){ mainWindow.webContents.send('showToast', e); }  
});

//Changement de canal d'un appareil
ipcMain.on('setDeviceChannel', (event, deviceId, channelId) => {
  try{ 
    dmxScene.setChannel(deviceId, channelId);
    mainWindow.webContents.send('sceneChannelsInfo', dmxScene.getSceneInfo());
    mainWindow.webContents.send('showToast', "Device " + deviceId + " has been updated.");
  }
  catch (e){ mainWindow.webContents.send('showToast', e); }  
});

//Exécution d'un test sur un appareil
ipcMain.on('executeTest', (event, deviceId) => {
  try{ 
    dmxScene.runTest(deviceId);
    mainWindow.webContents.send('showToast', "The test is running on device " + deviceId);
  }
  catch (e){ mainWindow.webContents.send('showToast', e); }  
});

//Exécution de l'adressage automatique des appareils
ipcMain.on('automaticAddressing', (event) => {
  try {
    dmxScene.automaticAddressing();
    mainWindow.webContents.send('showToast', "Automatic addressing performed.");
    mainWindow.webContents.send('sceneChannelsInfo', dmxScene.getSceneInfo());
  }
  catch (e){
    mainWindow.webContents.send('showToast', e);
  }
});

//Récupération des appareils
ipcMain.on('allDevices', (event) => {
  mainWindow.webContents.send('listDevices', deviceConfs.listAllConfigurations());
});

//Récupération des appareils pour affichage dans la scene
ipcMain.on('allDevicesScene', (event) => {
  mainWindow.webContents.send('listDevicesScene', deviceConfs.listAllConfigurations());
});

//Récupération des canaux scène
ipcMain.on('refreshSceneChannels', (event) => {
  let sceneInfo = dmxScene.getSceneInfo();
  mainWindow.webContents.send('sceneChannelsInfo', sceneInfo);
});

let sequences = [];

//Séquence test
/*dmxScene.addDevice("mini_strobe_rgb");
dmxScene.addDevice("mini_strobe_rgb");
dmxScene.automaticAddressing();
let sequence = new dmxSequencesManager(dmxScene, "My sequence 1");
sequence.addFrame(0, [255, 0, 0, 0, 0, 0, 251, 0]);
sequence.addFrame(1, [255, 0, 0, 0, 0, 0, 254, 0]);
sequences.push(sequence);*/

//Envoi des séquences au séquenceur
ipcMain.on('allSequences', (event) => {
  listSequences();
  mainWindow.webContents.send('showToast', "All sequences have been loaded.");
});

function listSequences(){
  let sequencesString = [];
  sequences.forEach(sequence => {
    let seqData = sequence.data.getSequenceData();
    seqData.key = sequence.key;
    sequencesString.push(seqData);
  });
  mainWindow.webContents.send('listSequences', JSON.stringify(sequencesString));
}

function loadSequence(sequenceId){
  let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
  let seqData = sequence.data.getSequenceData();
  seqData.key = sequence.key;
  mainWindow.webContents.send('sequenceData', JSON.stringify(seqData));
}

//Ajoute une nouvelle séquence
ipcMain.on('addSequence', (event) => {
  let sequence = new dmxSequencesManager(dmxScene);
  sequences.push({data: sequence, key: ""});
  mainWindow.webContents.send('showToast', "The sequence " + sequence.name + " has been created.");
  listSequences();
  loadSequence(sequence.id);
});

//Sélectionne une séquence
ipcMain.on('selectSequence', (event, sequenceId) => {
  loadSequence(sequenceId);
});

//Mettre à jour une séquence
ipcMain.on('updateSequence', (event, sequenceId, sequenceName, sequenceKey) => {
  let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
  sequence.key = sequenceKey;
  sequence.data.name = sequenceName;
  mainWindow.webContents.send('showToast', "The sequence " + sequence.data.name + " has been updated.");
  loadSequence(sequenceId);
  listSequences();
});

//Supprime une séquence
ipcMain.on('deleteSequence', (event, sequenceId) => {
  let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
  sequences.splice(sequences.indexOf(sequence), 1);
  mainWindow.webContents.send('showToast', "The sequence " + sequence.data.name + " has been deleted.");
  listSequences();
});

//Joue une séquence
ipcMain.on('playSequence', (event, sequenceId) => {
  let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
  sequence.data.play(dmxBox);
});
//Joue une séquence avec une touche
ipcMain.on('playSequenceWithKey', (event, key) => {
  let sequence = sequences.find(sequence => sequence.key == key);
  if(sequence != undefined) sequence.data.play(dmxBox);
});

function loadFrame(sequenceId, frameId){
  let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
  let frame = sequence.data.frames.find(frame => frame.id == frameId);
  mainWindow.webContents.send('frameData', sequenceId, JSON.stringify(frame.getFrameData()), JSON.stringify(dmxScene.getAvailableDevicesInfo()));
}

//Ajoute une frame à une séquence
ipcMain.on('addFrame', (event, sequenceId) => {
  let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
  sequence.data.addFrame();
  mainWindow.webContents.send('showToast', "The frame has been added to the sequence.");
  listSequences();
  loadFrame(sequenceId, sequence.data.frames[sequence.data.frames.length - 1].id);
});

//Sélectionne une frame d'une séquence
ipcMain.on('selectFrame', (event, sequenceId, frameId) => {
  loadFrame(sequenceId, frameId);
});

//Récupére la description des canaux d'un appareil
ipcMain.on('availableChannels', (event, deviceId, values) => {
  try {
    mainWindow.webContents.send('channelsData', JSON.stringify(dmxScene.getAvailableChannelsInfo(deviceId)), values);
  }
  catch (e){
    mainWindow.webContents.send('showToast', e);
  }
});

//Mise à jour de la valeur d'un canal en direct
ipcMain.on('updateChannelValue', (event, channelId, value) => {
  dmxBox.write(parseInt(channelId), parseInt(value));
});

//Supprimer une frame d'une séquence
ipcMain.on('deleteFrame', (event, sequenceId, frameId) => {
  let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
  let frame = sequence.data.frames.find(frame => frame.id == frameId);
  sequence.data.frames.splice(sequence.data.frames.indexOf(frame), 1);
  mainWindow.webContents.send('showToast', "The frame has been deleted from the sequence.");
  listSequences();
});

//Mis à jour d'une frame
ipcMain.on('updateFrame', (event, sequenceId, frameId, name, deviceId, values) => {
  try {
    let sequence = sequences.find(sequence => sequence.data.id == sequenceId);
    let frame = sequence.data.frames.find(frame => frame.id == frameId);
    frame.name = name;
    frame.setDevice(dmxScene, deviceId, values);
    mainWindow.webContents.send('showToast', "The frame has been updated.");
    listSequences();
    loadFrame(sequenceId, frameId);
  }
  catch (e){
    mainWindow.webContents.send('showToast', e);
  }
});

//Sauvegarder un projet
ipcMain.on('saveProject', (event) => {
  //Récupère la date et l'heure dans une variable
  let date = new Date();
  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let hour = date.getHours();
  let minute = date.getMinutes();
  let second = date.getSeconds();
  let fileName = "DMXBox_" + day + "-" + month + "-" + year + "_" + hour + "-" + minute + "-" + second + ".dmxbox";
  let filePath = dialog.showSaveDialogSync(mainWindow, {
    title: "Save your project",
    defaultPath: fileName,
    filters: [
      { name: 'DMXConnect Project', extensions: ['dmxproj'] }
    ]
  });
  //Préparation des données à sauvegarder
  let data = { scene: dmxScene, sequences: sequences };
  if(filePath != undefined) {
    fs.writeFileSync(filePath, JSON.stringify(data));
    mainWindow.webContents.send('showToast', "The project has been saved.");
  }
});

//Ouvrir un projet
ipcMain.on('openProject', (event) => {
  let filePath = dialog.showOpenDialogSync(mainWindow, {
    title: "Open a project",
    filters: [
      { name: 'DMXConnect Project', extensions: ['dmxproj'] }
    ]
  });
  if(filePath != undefined) {
    let data = JSON.parse(fs.readFileSync(filePath[0]));
    dmxScene = new dmxSceneManager(dmxBox);
    dmxScene.openScene(data.scene);
    sequences = [];
    data.sequences.forEach(sequence => {
      let newSequence = new dmxSequencesManager(dmxScene, sequence.data.name);
      newSequence.openSequence(sequence.data);
      sequences.push({data: newSequence, key: sequence.key});
    });
    mainWindow.webContents.send('showToast', "The project has been opened.");
  }
});

//Réinitialisation d'un appareil
ipcMain.on('resetDevice', (event, deviceId) => {
  dmxScene.resetDevice(deviceId);
});

//Récupération des appareils
ipcMain.on('quit', (event) => {
  quit();
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') quit();
});

function quit(){
  //Extinction de tous les appareils
  dmxScene.resetAllDevices();
  app.quit();
}