const EventEmitter = require('events')
const { SerialPort } = require('serialport')

var DMXBoxManager = class extends EventEmitter {
    constructor(){
        super();
        this.boxPort = "";
        this.serialPort = "";
        this.channels = new Array(512).fill(0);
        this.isConnected = this.connectionStatus;
        this.write = this.writeChannel;
        this.checkConnection();
    }

    //Retourne si la box est connectée ou non
    connectionStatus(){
        if(this.boxPort != "") return true;
        else return false;
    }

    //Vérifie périodiquement la connexion avec la box
    checkConnection(){
        //Si la connexion était active, on vérifie si elle y est toujours
        if(this.boxPort != ""){
            let boxFounded = false;
            let currentPort = this.boxPort;
            SerialPort.list().then(ports => {
                ports.forEach(function(port) {
                   if(port.path == currentPort) boxFounded = true;
                });
                if(!boxFounded){
                    this.emit('boxDisconnected');
                    this.boxPort = "";
                    this.serialPort = "";
                }
            });
        }
        //Sinon, on vérifie si la box est présente pour s'y connecter
        else {
            let newPort = this.boxPort;
            SerialPort.list().then(ports => {
                ports.forEach(function(port) {
                   if(port.vendorId == "2A03") newPort = port.path;
                });
                this.boxPort = newPort;
                if(this.boxPort != ""){
                    this.initializeConnection();
                }
            });
        }
        setTimeout(this.checkConnection.bind(this), 1500);
    }

    //Initialize le port série
    initializeConnection(){
        if(this.boxPort != ""){
            console.log("DMXBox found on port " + this.boxPort + ".");
            //Création du port série
            this.serialPort = new SerialPort({
                path: this.boxPort,
                baudRate: 9600,
                autoOpen: false
            });
            //Ouverture du port série
            this.serialPort.open(function(err){
                if(err) throw new Error("Unable to open the serial port.");
                else console.log("Port have been opened. Waiting for DMXBox...");
            });
            //Ecoute des réponses
            let versionString = "";
            let getVersion = false;
            this.serialPort.on('data', (data) => {
                //Réception d'un signal de la dmxbox
                //On est en train de lire la version du micrologiciel
                if(getVersion){
                    let incoming = data.toString();
                    for(let i = 0; i < incoming.length; i++){
                        if(incoming[i] == "\n"){
                            console.log("Version du micrologiciel : " + versionString);
                            getVersion = false;
                            versionString = "";
                        } 
                        else versionString += incoming[i];
                    }
                }
                //On n'est pas en train de lire la version du micrologiciel
                else {
                    if(data[0] == 0xA) this.emit('boxConnected');
                    else if(data[0] == 0xE) this.emit('commandSucceeded');
                    else if(data[0] == 0xD) getVersion = true;
                }
            });
        }
    }

    //Get the DMXBox version
    getVersion(){
        if(this.boxPort != ""){
            this.serialPort.write([0xD], function(err){
                if(err) throw new Error("Unable to read the DMXBox version.");
                else return true;
            });
        }
    }

    //Ecrire une valeur dans un canal
    writeChannel(channel, value){
        //On ne met à jour le canal que si la valeur est différente de l'état actuel
        if(this.channels[channel] != value) {
            if(this.boxPort != ""){
                this.serialPort.write([0xC, channel, value], function(err) {
                    if (err) throw new Error("Unable to set the channel value.");
                    else return true;
                });
                this.channels[channel] = value;
            }
        }
    }
}

module.exports = DMXBoxManager;