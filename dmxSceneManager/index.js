var deviceConfigurations = require('../deviceConfigurationsManager')

var deviceConfs = new deviceConfigurations();

/*Classe pour la gestion d'une scène
- Ajout d'un appareil
- Suppression d'un appareil
- Attribution d'une adresse
- */
var SceneManager = class {
    constructor(box){
        this.name = "My scene";
        this.maxChannels = 512;
        this.dmxDevices = [];
        this.dmxBox = box;
    }

    //Ajoute un appareil à la scène
    addDevice(slug){
        let dmxDevice = {};
        let deviceConfiguration = deviceConfs.getConfiguration(slug);
        if(deviceConfiguration){
            if(deviceConfiguration.channelModes.length > 0){
                dmxDevice.configuration = deviceConfiguration;
                dmxDevice.channelMode = 0;
                dmxDevice.channel = 1;
                dmxDevice.x = 50;
                dmxDevice.y = 50;
                this.dmxDevices.push(dmxDevice);
                return this.dmxDevices.length-1;
            }
            else throw new Error("This device is not configured with a channel mode.");
        }
        else throw new Error("This device doesn't exist.");
    }   
    
    //Retire un appareil de la scène
    removeDevice(id){
        if(this.dmxDevices[id] != undefined) {
            this.dmxDevices.splice(id, 1);
            return true;
        }
        else throw new Error("This device doesn't exist.");
    }

    //Change the channel mode of a device
    setChannelMode(id, channelMode){
        if(this.dmxDevices[id] != undefined){
            if(this.dmxDevices[id].configuration.channelModes[channelMode] != undefined) {
                this.dmxDevices[id].channelMode = channelMode;
                return true;
            }
            else throw new Error("This channel mode doesn't exist on this device.");
        }
        else throw new Error("This device doesn't exist.");
    }

    //Paramètre le canal de l'appareil
    setChannel(id, channel){
        if(this.dmxDevices[id] != undefined){
            if(channel >= 1 && channel <= this.maxChannels) {
                this.dmxDevices[id].channel = channel;
                return true;
            }
            else throw new Error("The wanted channel is out of range.")
        }
        else throw new Error("This device doesn't exist.");
    }

    //Attribue une adresse à chaque appareil DMX
    automaticAddressing(){
        let currentChannel = 1;
        this.dmxDevices.forEach((dmxDevice) => {
            //Si l'ensemble des 512 canaux sont utilisés
            if(currentChannel > this.maxChannels) throw new Error("The scene already use the " + this.maxChannels + " channels. Please remove devices or change channel modes of some devices.");
            //Sinon on peut continuer à adresser
            else {
                dmxDevice.channel = currentChannel;
                currentChannel += dmxDevice.configuration.channelModes[dmxDevice.channelMode].channels.length;
            }
        });
    }

    //Vérification des conflits canaux
    checkConflicts(){
        //Ordre des appareils pour avoir un numéro de canal croissant
        let availableChannels = [];
        for(let i = 0; i < this.maxChannels; i++){
            availableChannels[i] = true;
        } 
        var conflicts = [];
        //On vérifie les canaux utilisés pour chaque appareil
        this.dmxDevices.forEach((dmxDevice) => {
            let channel = dmxDevice.channel;
            let channelsNumber = dmxDevice.configuration.channelModes[dmxDevice.channelMode].channels.length;
            for(let i = channel; i < channelsNumber + channel; i++){
                if(availableChannels[i-1]) availableChannels[i-1] = false;
                else conflicts.push(i);
            }
        });
        return conflicts;
    }

    //Récupère les réglages d'un appareil DMX
    getDevice(id){
        if(this.dmxDevices[id] != undefined) {
            let data = {};
            data.id = id;
            data.device = this.dmxDevices[id];            
            return data;
        }
        else throw new Error("This device doesn't exist.");
    }

    //Récupère les infos de canaux
    getSceneInfo(){
        let data = {};
        let devices = [];
        this.dmxDevices.forEach((dmxDevice) => {
            devices.push({ channel: dmxDevice.channel, slug: dmxDevice.configuration.slug, x: dmxDevice.x, y:dmxDevice.y })
        });
        data.devices = devices;
        data.conflicts = this.checkConflicts();
        data.maxChannel = this.maxChannels;
        return data;
    }

    //Récupère les infos d'appareils pour affichage dans le séquenceur
    getAvailableDevicesInfo(){
        let devices = [];
        let id = 0;
        this.dmxDevices.forEach((dmxDevice) => {
            devices.push({ id: id, name: dmxDevice.configuration.name, channel: dmxDevice.channel });
            id++;
        });
        return devices;
    }

    //Récupère les infos de canaux pour affichage dans le séquenceur
    getAvailableChannelsInfo(deviceId){
        if(this.dmxDevices[deviceId] != undefined) {
            let data = {address: this.dmxDevices[deviceId].channel, channels: []};
            data.channels = this.dmxDevices[deviceId].configuration.channelModes[this.dmxDevices[deviceId].channelMode].channels; 
            return data;          
        }
        else throw new Error("This device doesn't exist.");
    }

    resetDevice(id){
        if(this.dmxDevices[id] != undefined){
            //Réinitialisation de l'ensemble des canaux de l'appareil
            let channelsNumber = this.dmxDevices[id].configuration.channelModes[this.dmxDevices[id].channelMode].channels.length;
            let channel = this.dmxDevices[id].channel;
            for(let i = 0; i < channelsNumber; i++){
                this.dmxBox.write(i + channel, 0);
            }
        }
        else throw new Error("This device doesn't exist.");
    }

    //Move a device on the scene
    moveDevice(id, x, y){
        if(this.dmxDevices[id] != undefined){
            this.dmxDevices[id].x = x;
            this.dmxDevices[id].y = y;
        }
        else throw new Error("This device doesn't exist.");
    }

    //Reset all devices
    resetAllDevices(){
        let i = 0;
        this.dmxDevices.forEach((dmxDevice) => {
            this.resetDevice(i);
            i++;
        });
    }

    //Exécute un test pour identifier l'appareil
    runTest(id){
        //DMXBox connectée
        if(this.dmxBox.isConnected()){
            //L'appareil a été trouvé
            if(this.dmxDevices[id] != undefined){
                let channelMode = this.dmxDevices[id].channelMode;
                let channelNumber = this.dmxDevices[id].channel;
                let channelId = 0;
                let testInfo = {};
                this.dmxDevices[id].configuration.channelModes[channelMode].channels.forEach((channel)=>{
                    channel.ranges.forEach((range)=>{
                        //Si la plage de canal parcourue est enregistrée en tant que test, on exécute le test dessus
                        if(range.isTestRange){
                            let channelToChange = channelNumber + channelId;
                            testInfo.channel = channelToChange;
                            testInfo.from = range.fromValue;
                            testInfo.to = range.toValue;
                        }
                    });
                    channelId ++;
                });

                if(testInfo.channel == undefined) throw new Error("This device doesn't have a test channel.");
                else {
                    try{
                        this.dmxBox.write(testInfo.channel, testInfo.to);
                        setTimeout(() => {
                            this.dmxBox.write(testInfo.channel, testInfo.from);
                            setTimeout(() => {
                                this.dmxBox.write(testInfo.channel, testInfo.to);
                                setTimeout(() => {
                                    this.dmxBox.write(testInfo.channel, testInfo.from);
                                }, 500);
                            }, 500);
                        }, 500);
                    }
                    catch (e){
                        throw new Error('Unable to run the test on the device.');
                    }
                }
            }
            else throw new Error("This device doesn't exist.");
        }
        //DMXBox déconnectée
        else throw new Error("Please connect the DMXBox and try again.");
    }

    //Charger une scène existante
    openScene(scene){
        this.name = scene.name;
        this.maxChannels = scene.maxChannels;
        scene.dmxDevices.forEach((dmxDevice) => {
            let fullDevice = {};
            fullDevice.configuration = dmxDevice.configuration;
            fullDevice.channelMode = dmxDevice.channelMode;
            fullDevice.channel = dmxDevice.channel;
            fullDevice.x = dmxDevice.x;
            fullDevice.y = dmxDevice.y;
            this.dmxDevices.push(fullDevice);
        });
    };
}

module.exports = SceneManager;