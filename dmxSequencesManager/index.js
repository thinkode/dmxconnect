/*Classe pour la gestion des séquences DMX
- Ajouter une séquence DMX
- Retirer une séquence DMX
- */

let frameId = 0;
var DmxFrame = class {
    constructor(sceneManager, name, deviceID, values){
        if(deviceID != -1 && values != undefined)
        {
            try{
                //On vérifie si l'appareil existe
                this.deviceData = sceneManager.getDevice(deviceID);
                //On vérifie que le nombre de valeurs passées correspond à la définition du mode de channel utilisé
                if(values.length == this.deviceData.device.configuration.channelModes[this.deviceData.device.channelMode].channels.length) this.values = values;
                else throw new Error("The values doesn't match the device configuration.");
            }
            catch (e){
                throw new Error('Unable to create the configuration.');
            }
        }    
        else if(deviceID == -1) {
            this.deviceData = {"id": -1, "device": {"name": "No device", "channel": "-", "configuration": {"name": "No linked device", "channelModes": [{"name": "No mode", "channels": []}]}}};
            this.values = [];    
        }
        this.id = frameId++;
        this.name = (name != undefined) ? name : "My frame " + this.id;
    }

    //Lie un appareil à la frame
    setDevice(sceneManager, deviceID, values){
        if(deviceID != -1 && values != undefined)
        {
            try{
                //On vérifie si l'appareil existe
                this.deviceData = sceneManager.getDevice(deviceID);
                //On vérifie que le nombre de valeurs passées correspond à la définition du mode de channel utilisé
                if(values.length == this.deviceData.device.configuration.channelModes[this.deviceData.device.channelMode].channels.length) this.values = values;
                else throw new Error("The values doesn't match the device configuration.");
            }
            catch (e){
                throw new Error('Unable to link the device.');
            }
        }    
        else if(deviceID == -1) {
            this.deviceData = {"id": -1, "device": {"name": "No device", "channel": "-", "configuration": {"name": "No linked device", "channelModes": [{"name": "No mode", "channels": []}]}}};
            this.values = [];    
        }
    }

    //Récupère les valeurs de la frame
    getFrameData(){
        return {"id": this.id, "name": this.name, "deviceId": this.deviceData.id, "values": this.values};
    }

    //Envoyer la configuration à l'appareil
    sendConfiguration(dmxBox){
        if(this.deviceData.id != -1) {
            let startChannel = this.deviceData.device.channel;
            let valueIndex = 0;
            for(let i = startChannel; i < startChannel + this.values.length; i++){
                dmxBox.write(i, this.values[valueIndex]);
                valueIndex++;
            }
        }
    }
}

let sequenceId = 0;

var DmxSequence = class {
    constructor(sceneManager, name){
        this.id = sequenceId++;
        this.name = (name != undefined) ? name : "My sequence " + this.id;
        this.sceneManager = sceneManager;
        this.frames = [];
    }

    addFrame(deviceID, values){
        if(deviceID == undefined) deviceID = -1;
        try{
            let dmxFrame = new DmxFrame(this.sceneManager, undefined, deviceID, values);
            this.frames.push(dmxFrame);
        }
        catch(e){
            throw new Error("Unable to add the DMX frame to the sequence");
        }
    }

    deleteFrame(index){
        if(index >= 0 && index <= this.frames.length-1){
            //Suppression de la frame
            this.frames[index] = undefined;
            //Réorganisation du tableau
            let newFrames = [];
            this.frames.forEach((frame) => {
                if(frame != undefined) newFrames.push(frame);
            });
            this.frames = newFrames;
        }
        else throw new Error("This frame cannot be deleted.");
    }

    getFrames(){
        return this.frames;
    }

    getFrame(index){
        if(index >= 0 && index <= this.frames.length-1){
            return this.frames[index];
        }
        else throw new Error("This frame cannot be got.");
    }

    setFrame(index, name, deviceID, values){
        if(index >= 0 && index <= this.frames.length-1){
            //Remplacement de la frame dans le tableau
            try{
                let dmxFrame = new DmxFrame(this.sceneManager, deviceID, values);
                dmxFrame.name = name;
                this.frames[index] = dmxFrame;
            }
            catch(e){
                throw new Error("This frame cannot be updated.");
            }
        }
        else throw new Error("This frame cannot be updated.");
    }

    play(dmxBox){
        //On joue la frame
        this.frames.forEach((frame) => {
            frame.sendConfiguration(dmxBox);
        });
    }

    send(dmxBox){
        this.frames.forEach((frame) => {
            frame.sendConfiguration(dmxBox);
        });
    }

    getSequenceData(){
        return {
            id: this.id,
            name: this.name,
            frames: this.frames
        }
    }

    openSequence(sequenceData){
        this.id = sequenceData.id;
        sequenceId = this.id + 1;
        sequenceData.frames.forEach((frame) => {
            let dmxFrame = new DmxFrame(this.sceneManager, frame.name, frame.deviceData.id, frame.values);
            dmxFrame.id = frame.id;
            frameId = dmxFrame.id + 1;
            this.frames.push(dmxFrame);
        });
    }
}

let sequencesNumber = 0;

module.exports = DmxSequence;